package main.java.com.company;

import main.java.com.company.model.SymbolTable;
import main.java.com.company.model.adt.HashTableTokenPosition;

public class Main {

    public static void main(String[] args) {
        SymbolTable symbolTable = new SymbolTable();
        symbolTable.add("ab");
        symbolTable.add("ba");
        symbolTable.add("b");

        HashTableTokenPosition position1 = symbolTable.getKey("ba");
        HashTableTokenPosition position2 = symbolTable.getKey("ab");
        System.out.println(position1);
        System.out.println(position2);
    }
}
