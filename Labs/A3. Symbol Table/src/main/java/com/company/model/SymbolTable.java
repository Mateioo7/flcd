package main.java.com.company.model;

import main.java.com.company.model.adt.HashTable;
import main.java.com.company.model.adt.HashTableTokenPosition;

public class SymbolTable {
    private final HashTable hashTable = new HashTable();

    public HashTableTokenPosition add(String token) {
        return hashTable.add(token);
    }

    public HashTableTokenPosition getKey(String token) {
        return hashTable.getKey(token);
    }
}
