package main.java.com.company.model.adt;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

public class HashTable {
    private final Map<Integer, List<String>> map = new HashMap<>();

    public HashTableTokenPosition add(String token) {
        int hash = hash(token);
        if (!map.containsKey(hash)) {
            map.put(hash, new LinkedList<>());
        }

        map.get(hash).add(token);
        return getKey(token);
    }

    public HashTableTokenPosition getKey(String token) {
        int hash = hash(token);
        int listIndex = getListIndex(token);

        return new HashTableTokenPosition(hash, listIndex);
    }

    private Integer hash(String token) {
        return token.chars().sum();
    }

    private Integer getListIndex(String token) {
        int hash = hash(token);
        return IntStream.range(0, map.get(hash).size())
                .filter(index -> token.equals(map.get(hash).get(index)))
                .findFirst().orElse(-1);
    }
}
