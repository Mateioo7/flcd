package main.java.com.company.model.adt;

public class HashTableTokenPosition {
    private final Integer hash;
    private final Integer listIndex;

    public HashTableTokenPosition(Integer hashKey, Integer listIndex) {
        this.hash = hashKey;
        this.listIndex = listIndex;
    }

    public Integer getHash() {
        return hash;
    }

    public Integer getListIndex() {
        return listIndex;
    }

    @Override
    public String toString() {
        return "HashTableTokenPosition{" +
                "hashKey=" + hash +
                ", listIndex=" + listIndex +
                '}';
    }
}
