package com.company.view;

import com.company.model.Grammar;
import com.company.model.LR0Parser;

import java.util.Scanner;

public class Console {
    private final Scanner scanner = new Scanner(System.in);

    public void run() {
        Grammar grammar = new Grammar("seminar.in");
        LR0Parser parser = new LR0Parser(grammar);

        boolean running = true;
        while (running) {
            System.out.print("\n");
            showMenu();
            int option = getInputInteger("Choose an option: ");
            System.out.print("\n");

            switch (option) {
                case 0:
                    running = false;
                    break;
                case 1:
                    String fileName = getInputString("Give the file name: ");
                    grammar = new Grammar(fileName);
                    break;
                case 2:
                    System.out.println(grammar.getNonTerminals());
                    break;
                case 3:
                    System.out.println(grammar.getTerminals());
                    break;
                case 4:
                    System.out.println(grammar.getStartSymbol());
                    break;
                case 5:
                    System.out.println(grammar.getProductions());
                    break;
                case 6:
                    String nonTerminal = getInputString("Give the non terminal: ");
                    System.out.println(grammar.getProductionsOfNonTerminal(nonTerminal));
                    break;
                case 7:
                    System.out.println(grammar.isContextFree());
                    break;
                case 8:
                    String sequence = getInputString("Give the sequence: ");
                    parser.parse(sequence);
                    break;
                default:
                    System.out.println("Please enter a valid option.");
            }
        }
    }

    private int getInputInteger(String message) {
        System.out.print(message);
        if (scanner.hasNextInt()) {
            return scanner.nextInt();
        }

        scanner.nextLine();
        return -1;
    }

    private String getInputString(String message) {
        System.out.print(message);
        return scanner.next();
    }

    private void showMenu() {
        System.out.println("0. Exit");
        System.out.println("1. Read grammar from file");
        System.out.println("2. Display the set of nonterminals");
        System.out.println("3. Display the set of terminals");
        System.out.println("4. Display the start symbol");
        System.out.println("5. Display the set of productions");
        System.out.println("6. Display the set of productions for a given nonterminal");
        System.out.println("7. Check if grammar is context free");
        System.out.println("8. Check sequence");
    }
}
