package com.company.model;

import java.util.List;

public class CanonicalCollection {
    private final List<ParserState> states;

    public CanonicalCollection(List<ParserState> states) {
        this.states = states;
    }

    public boolean contains(ParserState state) {
        for (ParserState parserState : states) {
            if (parserState.equals(state)) {
                return true;
            }
        }
        return false;
    }

    public List<ParserState> getStates() {
        return states;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (ParserState state : states) {
            sb.append(state.toString()).append("\n");
        }
        sb.setLength(sb.length() - 1);

        return sb.toString();
    }
}
