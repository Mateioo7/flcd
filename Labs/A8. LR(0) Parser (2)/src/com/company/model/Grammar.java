package com.company.model;

import com.company.utils.FileUtils;
import com.company.utils.MapUtils;

import java.util.*;

// A (formal) grammar is a 4-tuple: G=(N,Σ,P,S)
//        with the following meanings:
//        • N – set of nonterminal symbols and |N| < ∞
//        • Σ - set of terminal symbols (alphabet) and |Σ|<∞
//        • P – finite set of productions (rules), with the propriety: P⊆(N∪Σ)∗ N(N∪Σ)∗ X(N∪Σ)∗
//        • S∈N – start symbol /axiom
public class Grammar {
    private final List<String> nonTerminals = new ArrayList<>();
    private final List<String> terminals = new ArrayList<>();
    private final Map<String, List<List<String>>> productions = new HashMap<>();
    private int currentIndex = 0;
    private final Map<List<String>, Integer> productionIndexes = new HashMap<>();
    private String startSymbol;

    public Grammar(String fileName) {
        this.readFromFile(fileName);
    }

    private void readFromFile(String fileName) {
        List<String> fileLines = FileUtils.getFileLines(fileName);

        int nonTerminalsLineIndex = fileLines.indexOf("NONTERMINALS");
        int terminalsLineIndex = fileLines.indexOf("TERMINALS");
        int startSymbolLineIndex = fileLines.indexOf("START SYMBOL");
        int productionsLineIndex = fileLines.indexOf("PRODUCTIONS");

        for (int i = nonTerminalsLineIndex + 1; i < terminalsLineIndex; i++) {
            nonTerminals.add(fileLines.get(i));
        }

        for (int i = terminalsLineIndex + 1; i < startSymbolLineIndex; i++) {
            terminals.add(fileLines.get(i));
        }

        startSymbol = fileLines.get(startSymbolLineIndex + 1);

        for (int i = productionsLineIndex + 1; i < fileLines.size(); i++) {
            String[] splitByArrow = fileLines.get(i).split("->");
            String nonTerminal = splitByArrow[0].strip();
            String rules = splitByArrow[1].strip();
            String[] ruleList = rules.split("\\|");

            if (!productions.containsKey(nonTerminal)) {
                productions.put(nonTerminal, new ArrayList<>());
            }

            for (String rule : ruleList) {
                rule = rule.strip();
                String[] splitBySpace = rule.split(" ");
                List<String> tokens = new ArrayList<>(Arrays.asList(splitBySpace));
                productions.get(nonTerminal).add(tokens);
                productionIndexes.put(tokens, ++currentIndex);
            }
        }
    }

    public boolean isContextFree() {
        for (String nonTerminal : productions.keySet()) {
            if (nonTerminal.split(" ").length > 1) {
                return false;
            }
        }

        return true;
    }

    public List<List<String>> getProductionsOfNonTerminal(String nonTerminal) {
        return productions.get(nonTerminal);
    }

    public List<String> getNonTerminals() {
        return nonTerminals;
    }

    public List<String> getTerminals() {
        return terminals;
    }

    public Map<String, List<List<String>>> getProductions() {
        return productions;
    }

    public String getStartSymbol() {
        return startSymbol;
    }

    public boolean isNonTerminal(String token) {
        return nonTerminals.contains(token);
    }

    public Map<List<String>, Integer> getProductionIndexes() {
        return productionIndexes;
    }

    public List<String> getProductionByIndex(int index) {
        for (List<String> production : productionIndexes.keySet()) {
            if (productionIndexes.get(production) == index) {
                return production;
            }
        }

        return new ArrayList<>();
    }

    public String getProductionSource(List<String> productionToFind) {
        for (String source : productions.keySet()) {
            for (List<String> production : productions.get(source)) {
                if (production.equals(productionToFind)) {
                    return source;
                }
            }
        }

        return "";
    }

    public String getPrettyProductionIndexes() {
        Map<List<String>, Integer> sortedProductionIndexes = MapUtils.sortByValue(productionIndexes);
        StringBuilder sb = new StringBuilder();

        for (List<String> production : sortedProductionIndexes.keySet()) {
            sb.append(sortedProductionIndexes.get(production)).append(": ")
                    .append(getProductionSource(production)).append(" -> ")
                    .append(String.join("", production)).append("\n");
        }
        sb.setLength(sb.length() - 1);

        return sb.toString();
    }
}
