package com.company.model;

import java.util.HashMap;
import java.util.Map;

public class LR0Table {
    private final Map<Integer, String> stateActions = new HashMap<>();
    private final Map<Pair<Integer, String>, Integer> stateGoto = new HashMap<>();

    public Map<Integer, String> getStateActions() {
        return stateActions;
    }

    public Map<Pair<Integer, String>, Integer> getStateGoto() {
        return stateGoto;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Integer stateIndex : stateActions.keySet()) {
            sb.append("s").append(stateIndex).append(": ").append(stateActions.get(stateIndex)).append("\n");
        }

        for (Pair<Integer, String> pair : stateGoto.keySet()) {
            sb.append("goto(").append("s").append(pair.getFirst()).append(", ").append(pair.getSecond()).append(")")
                    .append(" = ").append(stateGoto.get(pair)).append("\n");
        }
        sb.setLength(sb.length() - 1);
        return sb.toString();
    }
}
