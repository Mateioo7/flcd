package com.company.model;

import java.util.List;
import java.util.Objects;

public class ParsedRule {
    private final String sourceNonTerminal;
    private final List<String> rules;
    private int dotIndex;

    public ParsedRule(String sourceNonTerminal, List<String> rules, int dotIndex) {
        this.sourceNonTerminal = sourceNonTerminal;
        this.rules = rules;
        this.dotIndex = dotIndex;
    }

    public ParsedRule(ParsedRule rule) {
        this.sourceNonTerminal = rule.sourceNonTerminal;
        this.rules = rule.rules;
        this.dotIndex = rule.dotIndex;
    }

    public String getTokenAfterDot() {
        if (!canBeParsed()) {
            return "";
        }

        return rules.get(dotIndex);
    }

    public boolean canBeParsed() {
        return dotIndex < rules.size();
    }

    public void moveDotIndex() {
        dotIndex++;
    }

    public String getSourceNonTerminal() {
        return sourceNonTerminal;
    }

    public List<String> getRules() {
        return rules;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ParsedRule that = (ParsedRule) o;
        return dotIndex == that.dotIndex && Objects.equals(sourceNonTerminal, that.sourceNonTerminal) && Objects.equals(rules, that.rules);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sourceNonTerminal, rules, dotIndex);
    }

    @Override
    public String toString() {
        StringBuilder production = new StringBuilder();
        for (int i = 0; i < rules.size(); i++) {
            if (i == dotIndex) {
                production.append(".");
            }
            production.append(rules.get(i));
        }
        if (dotIndex == rules.size()) {
            production.append(".");
        }

        return sourceNonTerminal + " -> " + production;
    }
}
