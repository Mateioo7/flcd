package com.company.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class LR0Parser {
    private final Grammar grammar;
    private final CanonicalCollection canonicalCollection;
    private final LR0Table lr0Table;
    private boolean conflictFound = false;

    public LR0Parser(Grammar grammar) {
        this.grammar = grammar;
        canonicalCollection = new CanonicalCollection(new ArrayList<>());
        lr0Table = new LR0Table();
    }

    public void parse(String sequence) {
        computeCanonicalCollectionAndLR0Table();
        System.out.println("\nLR(0) Table:");
        System.out.println(lr0Table);

        if (conflictFound) {
            System.out.println("Conflict found while computing LR(0) Table. The grammar is not LR(0)");
            return;
        }
        computeParsingTree(sequence);
    }

    private void computeParsingTree(String sequence) {
        ArrayList<String> workingStack = new ArrayList<>(List.of("0"));
        ArrayList<String> inputStack = new ArrayList<>(Arrays.asList(sequence.split("")));
        List<String> inputStackCopy = new ArrayList<>(inputStack);

        System.out.println();
        for (String character : inputStack) {
            System.out.println("Input stack: " + String.join("", inputStackCopy) + "$");
            System.out.println("Working stack: $" + String.join("", workingStack));

            int index = Integer.parseInt(workingStack.get(workingStack.size() - 1));
            workingStack.add(character);
            workingStack.add(String.valueOf(lr0Table.getStateGoto().get(Pair.of(index, character))));
            inputStackCopy.remove(character);
        }
        inputStack.clear();

        boolean succeeded = false;
        while (true) {
            System.out.println("Input stack: $" + String.join("", inputStack));
            System.out.println("Working stack: $" + String.join("", workingStack));

            String indexString = workingStack.get(workingStack.size() - 1);
            if (indexString.equals("null")) {
                break;
            }
            int index = Integer.parseInt(indexString);
            String reduceAction = lr0Table.getStateActions().get(index);

            if (reduceAction.equals("accept")) {
                System.out.println("Working stack: $" + reduceAction);
                succeeded = true;
                break;
            }

            int reduceIndex = Integer.parseInt(reduceAction.split(" ")[1]);

            List<String> productionToFind = grammar.getProductionByIndex(reduceIndex);
            int productionSize = productionToFind.size();

            List<String> production = new ArrayList<>();
            for (int i = workingStack.size() - 2 * productionSize; i < workingStack.size(); i = i + 2) {
                production.add(workingStack.get(i));
            }

            if (!productionToFind.equals(production)) {
                break;
            }

            workingStack.subList(workingStack.size() - 2 * productionSize, workingStack.size()).clear();
            inputStack.add(String.valueOf(reduceIndex));

            workingStack.add(grammar.getProductionSource(production));
            workingStack.add(String.valueOf(lr0Table.getStateGoto().get(
                    Pair.of(Integer.parseInt(workingStack.get(workingStack.size() - 2)),
                            workingStack.get(workingStack.size() - 1)))));
        }

        if (!succeeded) {
            System.out.println("Sequence not accepted");
            return;
        }

        Collections.reverse(inputStack);

        System.out.println("\nProductions:");
        System.out.println(grammar.getPrettyProductionIndexes());
        System.out.println("Order of derivation: " + String.join("", inputStack));

        ArrayList<String> derivations = new ArrayList<>(List.of(grammar.getStartSymbol()));
        for (String character : inputStack) {
            List<String> production = grammar.getProductionByIndex(Integer.parseInt(character));
            String source = grammar.getProductionSource(production);
            int sourceSize = source.length();
            String s;
            String productionString = String.join("", grammar.getProductionByIndex(Integer.parseInt(character)));
            s = derivations.get(derivations.size() - 1).substring(0,
                    derivations.get(derivations.size() - 1).length() - sourceSize) + productionString;
            derivations.add(s);
        }

        StringBuilder sb = new StringBuilder();
        for (String derivation : derivations) {
            sb.append(derivation).append(" => ");
        }
        sb.setLength(sb.length() - 4);

        System.out.println(sb);
    }

    private void computeCanonicalCollectionAndLR0Table() {
        ParsedRule initialParsedRule = new ParsedRule("S'", List.of(grammar.getStartSymbol()), 0);
        computeClosure(List.of(initialParsedRule));
    }

    private ParserState computeClosure(List<ParsedRule> parsedRules) {
        System.out.print("closure(" + parsedRules + ")");
        ParserState state = new ParserState(new ArrayList<>());
        for (ParsedRule parsedRule : parsedRules) {
            state.getParsedRules().add(parsedRule);

            if (!parsedRule.canBeParsed()) {
                continue;
            }

            if (grammar.isNonTerminal(parsedRule.getTokenAfterDot())) {
                String nonTerminal = parsedRule.getTokenAfterDot();
                List<List<String>> nonTerminalProductions = grammar.getProductionsOfNonTerminal(nonTerminal);
                for (List<String> productionRule : nonTerminalProductions) {
                    state.getParsedRules().add(new ParsedRule(nonTerminal, productionRule, 0));
                }
            }
        }

        if (canonicalCollection.contains(state)) {
            int duplicateStateIndex = canonicalCollection.getStates().indexOf(state);
            System.out.println(" = " + state + " = s" + duplicateStateIndex);

            return state;
        } else {
            canonicalCollection.getStates().add(state);
        }

        int stateIndex = canonicalCollection.getStates().indexOf(state);
        System.out.println(" = " + state + " = s" + stateIndex);

        for (ParsedRule parsedRule : parsedRules) {
            String stateAction = getStateAction(parsedRule);
            if (lr0Table.getStateActions().get(stateIndex) != null &&
                    !lr0Table.getStateActions().get(stateIndex).equals(stateAction)) {
                conflictFound = true;
            }

            lr0Table.getStateActions().put(stateIndex, stateAction);
        }

        goTo(state);

        return state;
    }

    private void goTo(ParserState state) {
        for (ParsedRule parsedRule : state.getParsedRules()) {
            if (parsedRule.canBeParsed()) {
                ParsedRule ruleCopy = new ParsedRule(parsedRule);
                String tokenAfterDot = ruleCopy.getTokenAfterDot();

                ruleCopy.moveDotIndex();

                int stateIndex = canonicalCollection.getStates().indexOf(state);
                System.out.print("goto(s" + stateIndex + ", " + tokenAfterDot + ") = ");
                lr0Table.getStateGoto().put(Pair.of(stateIndex, tokenAfterDot),
                        canonicalCollection.getStates().indexOf(computeClosure(List.of(ruleCopy))));
            }
        }
    }

    private String getStateAction(ParsedRule rule) {
        // if dot not at the end
        if (rule.canBeParsed()) {
            return "shift";
        }

        if (rule.getSourceNonTerminal().equals("S'")) {
            return "accept";
        }

        return "reduce " + grammar.getProductionIndexes().get(rule.getRules());
    }
}
