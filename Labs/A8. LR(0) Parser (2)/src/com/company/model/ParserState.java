package com.company.model;

import java.util.List;
import java.util.Objects;

public class ParserState {
    private final List<ParsedRule> parsedRules;

    public ParserState(List<ParsedRule> parsedRules) {
        this.parsedRules = parsedRules;
    }

    public List<ParsedRule> getParsedRules() {
        return parsedRules;
    }

    @Override
    public boolean equals(Object o) {
        boolean same = false;
        if (o instanceof ParserState) {
            same = this.parsedRules.equals(((ParserState) o).parsedRules);
        }

        return same;
    }

    @Override
    public int hashCode() {
        return Objects.hash(parsedRules);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        for (ParsedRule rule : parsedRules) {
            sb.append("[").append(rule).append("] ");
        }
        sb.setLength(sb.length() - 1);
        sb.append("}");

        return sb.toString();
    }
}
