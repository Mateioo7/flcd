I. Alphabet
a. English alphabet: [A-Za-z]
b. Decimal digits: (0-9);
c. Special characters: _


II. Lexic
a. Special symbols, representing:
- separators ( ) [ ] { } ; space newline
- operators + - * / % = < <= == != >= >
- reserved words: start read print int float char string array if else while for

b. Identifiers:
= a sequence of letters and digits, where the first character is a letter
identifier ::= letter{letter | digit}
letter ::= uppercase_letter | lowercase_letter
uppercase_letter ::= "A" | "B" | ... | "Z"
lowercase_letter ::= "a" | "b" | ... | "z"
digit ::= "0" | "1" | ... | "9"

c. Constants:
1. integer
integer ::= "0" | ["+" | "-"]?non_zero_digit{digit}
digit ::= "0" | non_zero_digit
non_zero_digit ::= "1" | ... | "9"

2. float
float ::= ["+" | "-"](non_zero_digit{digit}.digit{digit} | "0".digit{digit})
digit ::= "0" | non_zero_digit
non_zero_digit ::= "1" | ... | "9"

3. character
character ::= 'letter|digit'
letter ::= "a" | "b" | ... | "z" | "A" | "B" | ... | "Z"
digit ::= "0" | "1" | ... | "9"

4. string
string ::= "actual_string"
actual_string ::= {letter | digit | " "}
letter ::= "a" | "b" | ... | "z" | "A" | "B" | ... | "Z"
digit ::= "0" | "1" | ... | "9"


III. Syntax
program ::= "start" cmpd_stmt
cmpd_stmt ::= "{" stmt_list "}"
stmt_list ::= stmt | stmt ";" stmt_list
stmt ::= simple_stmt | struct_stmt

simple_stmt ::= (assign_stmt | io_stmt | declaration) ";"
assign_stmt ::= IDENTIFIER "=" expression
expression ::= expression ("+"|"-") term | term
term ::= term ("*"|"/","%") factor | factor
factor ::= "(" expression ")" | integer | IDENTIFIER | array_identifier
array_identifier ::= IDENTIFIER"["integer | IDENTIFIER"]"

integer ::= "0" | non_zero_digit{digit}
digit ::= "0" | non_zero_digit
non_zero_digit ::= "1" | ... | "9"

io_stmt ::= "read""("IDENTIFIER | array_identifier")" | "print""("IDENTIFIER | CONSTANT | array_identifier")"

declaration ::= type IDENTIFIER {"," IDENTIFIER}
type ::= simple_type | array_type
simple_type ::= "int" | "float" | "char" | "string"
array_type ::= simple_type "array""["greater_than_zero"]"
greater_than_zero ::= non_zero_digit{digit}
digit ::= "0" | non_zero_digit
non_zero_digit ::= "1" | ... | "9"

struct_stmt ::= cmpd_stmt | if_stmt | while_stmt | for_stmt
if_stmt ::= "if" condition cmpd_stmt ["else" cmpd_stmt]
condition ::= "("expression relation expression")"
relation ::= "<" | "<=" | "==" | "!=" | ">=" | ">"

while_stmt ::= "while" condition cmpd_stmt

for_stmt ::= "for" for_header cmpd_stmt
for_header ::= "("assign_stmt ";" condition ";" assign_stmt")"
