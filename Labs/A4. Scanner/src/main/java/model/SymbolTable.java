package model;

import model.adt.HashTable;
import model.adt.HashTableTokenPosition;
import model.adt.Pair;
import utils.FileUtils;

import java.util.List;
import java.util.stream.Collectors;

public class SymbolTable {
    private final HashTable hashTable = new HashTable();

    public HashTableTokenPosition add(String token) {
        return hashTable.add(token);
    }

    public HashTableTokenPosition getKey(String token) {
        return hashTable.getKey(token);
    }

    public void saveToFile(String fileName) {
        List<Pair<HashTableTokenPosition, String>> pairs =
                hashTable.entrySet().stream()
                        .flatMap(integerSetEntry -> integerSetEntry.getValue().stream()
                                .map(s -> new Pair<>(getKey(s), s)))
                        .collect(Collectors.toList());

        StringBuilder fileContent = new StringBuilder();
        for (Pair<HashTableTokenPosition, String> pair : pairs) {
            HashTableTokenPosition tokenPosition = pair.getKey();
            fileContent
                    .append("(hash=").append(tokenPosition.getHash())
                    .append(", index=").append(tokenPosition.getListIndex())
                    .append(") ").append("-> ").append(pair.getValue());
            fileContent.append("\n");
        }

        FileUtils.writeToFile(fileName, fileContent.toString());
    }
}
