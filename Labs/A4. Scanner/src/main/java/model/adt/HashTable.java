package model.adt;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.IntStream;

public class HashTable {
    private final Map<Integer, Set<String>> map = new HashMap<>();

    public HashTableTokenPosition add(String token) {
        int hash = hash(token);
        if (!map.containsKey(hash)) {
            map.put(hash, new HashSet<>());
        }

        map.get(hash).add(token);
        return getKey(token);
    }

    public HashTableTokenPosition getKey(String token) {
        int hash = hash(token);
        int listIndex = getListIndex(token);

        return new HashTableTokenPosition(hash, listIndex);
    }

    private Integer hash(String token) {
        return token.chars().sum();
    }

    private Integer getListIndex(String token) {
        int hash = hash(token);
        List<String> list = new ArrayList<>(map.get(hash));
        return IntStream.range(0, list.size())
                .filter(index -> token.equals(list.get(index)))
                .findFirst().orElse(-1);
    }

    public Set<Map.Entry<Integer, Set<String>>> entrySet() {
        return map.entrySet();
    }
}
