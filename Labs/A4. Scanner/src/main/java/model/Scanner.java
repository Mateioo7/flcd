package model;

import model.adt.HashTableTokenPosition;
import model.adt.Pair;
import utils.FileUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class Scanner {
    private final String inputFileName;
    private final SymbolTable symbolTable;
    private final List<Pair<String, HashTableTokenPosition>> programInternalForm;

    private final List<String> separators;
    private final List<String> operators;
    private final List<String> reservedWords;

    public Scanner(String inputFileName) {
        this.inputFileName = inputFileName;
        this.symbolTable = new SymbolTable();
        this.programInternalForm = new ArrayList<>();

        List<String> tokens = this.getTokensFromFile();
        this.separators = tokens.subList(0, 10);
        this.operators = tokens.subList(10, 22);
        this.reservedWords = tokens.subList(22, tokens.size());
    }

    public void performLexicalCheck() {
        List<String> programLines = FileUtils.getFileLines(inputFileName);
        boolean successful = true;
        int lineCount = 1;
        for (String programLine : programLines) {
            successful = tokenizeLine(programLine, lineCount);
            if (!successful) {
                break;
            }

            lineCount++;
        }

        if (successful) {
            System.out.println("Lexically correct");
            symbolTable.saveToFile(FileUtils.getBaseName(inputFileName) + "-symbol-table.txt");
            this.saveProgramInternalFormToFile(FileUtils.getBaseName(inputFileName) + "-program-internal-form.txt");
        } else {
            FileUtils.writeToFile(FileUtils.getBaseName(inputFileName) + "-symbol-table.txt", "");
            FileUtils.writeToFile(FileUtils.getBaseName(inputFileName) + "-program-internal-form.txt", "");
        }
    }


    private boolean tokenizeLine(String line, int lineIndex) {
        int index = 0;
        while (index < line.length()) {
            char currentCharacter = line.charAt(index);
            StringBuilder token = new StringBuilder();
            while (!isSeparator(Character.toString(currentCharacter))) {
                if (currentCharacter == '"') {
                    Pair<String, Integer> stringWithNewIndex = getString(line, index);
                    token = new StringBuilder(stringWithNewIndex.getKey());

                    index = stringWithNewIndex.getValue() - 1;
                    break;
                } else if (currentCharacter == '\'') {
                    Pair<String, Integer> characterWithNewIndex = getCharacter(line, index);
                    token = new StringBuilder(characterWithNewIndex.getKey());

                    index = characterWithNewIndex.getValue() - 1;
                    break;
                }

                token.append(currentCharacter);
                index++;
                if (index < line.length()) {
                    currentCharacter = line.charAt(index);
                } else {
                    break;
                }
            }

            // if directly found separator
            if (token.toString().equals("")) {
                if (currentCharacter != ' ') {
                    programInternalForm.add(new Pair<>(Character.toString(currentCharacter), new HashTableTokenPosition(-1, -1)));
                }

                index++;
                continue;
            }

            if (isOperator(token.toString()) || isReservedWord(token.toString())) {
                programInternalForm.add(new Pair<>(token.toString(), new HashTableTokenPosition(-1, -1)));
            } else if (isIdentifier(token.toString()) || isConstant(token.toString())) {
                programInternalForm.add(new Pair<>("identifier", symbolTable.add(token.toString())));
            } else {
                System.out.println("Lexical error at line " + lineIndex + ": " + line.strip());
                return false;
            }
            token.setLength(0);

            if (currentCharacter != ' ') {
                programInternalForm.add(new Pair<>(Character.toString(currentCharacter), new HashTableTokenPosition(-1, -1)));
            }

            index++;
        }
        return true;
    }

    private void saveProgramInternalFormToFile(String fileName) {
        StringBuilder fileContent = new StringBuilder();
        for (Pair<String, HashTableTokenPosition> pair : programInternalForm) {
            HashTableTokenPosition tokenPosition = pair.getValue();
            fileContent
                    .append(pair.getKey()).append(" -> ")
                    .append("(hash=").append(tokenPosition.getHash())
                    .append(", index=").append(tokenPosition.getListIndex()).append(")");
            fileContent.append("\n");
        }

        FileUtils.writeToFile(fileName, fileContent.toString());
    }

    private boolean isIdentifier(String word) {
        if (word.length() > 8) {
            return false;
        }

        Pattern pattern = Pattern.compile("^[a-zA-Z]+[0-9]*[a-zA-Z]*$");
        return pattern.matcher(word).matches();
    }

    private boolean isConstant(String word) {
        return isString(word) || isCharacter(word) || isInteger(word) || isFloat(word);
    }

    private boolean isInteger(String word) {
        if (word.length() > 8) {
            return false;
        }

        Pattern pattern = Pattern.compile("^(0|([+|-]?[1-9][0-9]*))$");
        return pattern.matcher(word).matches();
    }

    private boolean isFloat(String word) {
        if (word.length() > 8) {
            return false;
        }

        Pattern pattern = Pattern.compile("^[+|-]?((0.[0-9]+)|[1-9][0-9]*.[1-9]+)$");
        return pattern.matcher(word).matches();
    }

    private Pair<String, Integer> getCharacter(String line, int index) {
        int simpleQuotesCount = 0;
        StringBuilder string = new StringBuilder();

        while (index < line.length() && simpleQuotesCount < 2) {
            if (line.charAt(index) == '\'') {
                simpleQuotesCount++;
            }

            string.append(line.charAt(index));
            index++;
        }

        return new Pair<>(string.toString(), index);
    }

    private boolean isCharacter(String word) {
        if (word.length() > 3) {
            return false;
        }

        Pattern pattern = Pattern.compile("^'([a-zA-Z]|[0-9])'$");
        return pattern.matcher(word).matches();
    }

    private Pair<String, Integer> getString(String line, int index) {
        int doubleQuotesCount = 0;
        StringBuilder string = new StringBuilder();

        while (index < line.length() && doubleQuotesCount < 2) {
            if (line.charAt(index) == '"') {
                doubleQuotesCount++;
            }

            string.append(line.charAt(index));
            index++;
        }

        return new Pair<>(string.toString(), index);
    }

    private boolean isString(String word) {
        if (word.length() > 128) {
            return false;
        }

        Pattern pattern = Pattern.compile("^\"[a-zA-Z0-9 ]*\"$");
        return pattern.matcher(word).matches();
    }

    private boolean isSeparator(String word) {
        return separators.contains(word);
    }

    private boolean isOperator(String word) {
        return operators.contains(word);
    }

    private boolean isReservedWord(String word) {
        return reservedWords.contains(word);
    }

    private List<String> getTokensFromFile() {
        List<String> lines = FileUtils.getFileLines("tokens.txt");
        for (int i = 0; i < lines.size(); i++) {
            if (lines.get(i).equals("space")) {
                lines.set(i, " ");
            } else if (lines.get(i).equals("newline")) {
                lines.set(i, "\n");
            }
        }

        return lines;
    }
}
