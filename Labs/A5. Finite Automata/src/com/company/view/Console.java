package com.company.view;

import com.company.model.FiniteAutomata;

import java.util.Scanner;

public class Console {
    private final Scanner scanner = new Scanner(System.in);

    public void run() {
        FiniteAutomata finiteAutomata = new FiniteAutomata("fa.in");

        boolean running = true;
        while (running) {
            showMenu();
            int option = getInputInteger("Choose an option: ");

            switch (option) {
                case 0:
                    running = false;
                    break;
                case 1:
                    String fileName = getInputString("Give the file name: ");
                    finiteAutomata = new FiniteAutomata(fileName);
                    break;
                case 2:
                    System.out.println(finiteAutomata.getStates());
                    break;
                case 3:
                    System.out.println(finiteAutomata.getAlphabet());
                    break;
                case 4:
                    System.out.println(finiteAutomata.getInitialState());
                    break;
                case 5:
                    System.out.println(finiteAutomata.getFinalStates());
                    break;
                case 6:
                    System.out.println(finiteAutomata.getTransitions());
                    break;
                case 7:
                    System.out.println(finiteAutomata.isDeterministic());
                    break;
                case 8:
                    String sequence = getInputString("Give a sequence: ");
                    System.out.println(finiteAutomata.isSequenceAccepted(sequence));
                    break;
                default:
                    System.out.println("Please enter a valid option.");
            }
        }
    }

    private int getInputInteger(String message) {
        System.out.print(message);
        if (scanner.hasNextInt()) {
            return scanner.nextInt();
        }

        scanner.nextLine();
        return -1;
    }

    private String getInputString(String message) {
        System.out.print(message);
        return scanner.next();
    }

    private void showMenu() {
        System.out.println("0. Exit");
        System.out.println("1. Read FA from file");
        System.out.println("2. Display the set of states");
        System.out.println("3. Display the alphabet");
        System.out.println("4. Display the initial state");
        System.out.println("5. Display the set of final states");
        System.out.println("6. Display the transitions");
        System.out.println("7. Check if the FA is deterministic (DFA)");
        System.out.println("8. Check if a sequence is accepted.");
    }
}
