package com.company.utils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class FileUtils {
    public static List<String> getFileLines(String fileName) {
        List<String> lines = new ArrayList<>();
        File file = new File(fileName);
        String path = file.getAbsolutePath();
        try {
            return Files.readAllLines(Path.of(path));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return lines;
    }

    public static String getFileContent(String fileName) {
        return String.join("", getFileLines(fileName));
    }

    public static void writeToFile(String fileName, String content) {
        File file = new File(fileName);
        String path = file.getAbsolutePath();

        try {
            Files.write(Path.of(path), Collections.singleton(content));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String getBaseName(String fileName) {
        return fileName.split("\\.")[0];
    }
}
