package com.company;

import com.company.view.Console;

public class Main {
    public static void main(String[] args) {
        Console console = new Console();
        console.run();
    }
}
