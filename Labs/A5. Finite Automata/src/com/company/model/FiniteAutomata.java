package com.company.model;

import com.company.utils.FileUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//      Finite Automata is a 5-tuple M = (Q, Σ, δ, q0, F), where:
// • Q – finite set of states (|Q|<∞)
// • Σ – finite alphabet (|Σ|<∞)
// • δ – transition function : δ: Q×Σ→P(Q)
// • q0 – initial state q0 ∊ Q
// • F⊆Q – set of final states
public class FiniteAutomata {
    private String initialState;
    private final List<String> states = new ArrayList<>();
    private final List<String> alphabet = new ArrayList<>();
    // e.g: δ(p,a) = q; δ(p,a) = r => (p, a) = [q, r]
    private final Map<Pair<String, String>, List<String>> transitions = new HashMap<>();
    private final List<String> finalStates = new ArrayList<>();

    public FiniteAutomata(String fileName) {
        this.readFromFile(fileName);
    }

    private void readFromFile(String fileName) {
        List<String> fileLines = FileUtils.getFileLines(fileName);

        int statesLineIndex = 0;
        int alphabetLineIndex = fileLines.indexOf("Alphabet");
        for (int i = statesLineIndex + 1; i < alphabetLineIndex; i++) {
            states.add(fileLines.get(i));
        }

        int initialStateLineIndex = fileLines.indexOf("Initial State");
        for (int i = alphabetLineIndex + 1; i < initialStateLineIndex; i++) {
            alphabet.add(fileLines.get(i));
        }
        initialState = fileLines.get(initialStateLineIndex + 1);

        int finalStateLineIndex = fileLines.indexOf("Final States");
        int transitionsLineIndex = fileLines.indexOf("Transitions");
        for (int i = finalStateLineIndex + 1; i < transitionsLineIndex; i++) {
            finalStates.add(fileLines.get(i));
        }

        for (int i = transitionsLineIndex + 1; i < fileLines.size(); i++) {
            String[] splitByEqual = fileLines.get(i).split("=");
            String destination = splitByEqual[1].strip();

            String[] splitByComma = splitByEqual[0].split(",");
            String leftPartOfComma = splitByComma[0].strip();
            String source = leftPartOfComma.substring(leftPartOfComma.indexOf("(") + 1).strip();
            String rightPartOfComma = splitByComma[1].strip();
            String route = rightPartOfComma.substring(0, rightPartOfComma.indexOf(")")).strip();

            if (transitions.containsKey(Pair.of(source, route))) {
                List<String> destinations = transitions.get(Pair.of(source, route));
                if (!destinations.contains(destination)) {
                    destinations.add(destination);
                }
            } else {
                List<String> destinations = new ArrayList<>();
                destinations.add(destination);
                transitions.put(Pair.of(source, route), destinations);
            }
        }
    }

    public boolean isDeterministic() {
        for (Pair<String, String> key : transitions.keySet()) {
            if (transitions.get(key).size() > 1) {
                return false;
            }
        }

        return true;
    }

    public boolean isSequenceAccepted(String sequence) {
        if (!isDeterministic()) {
            return false;
        }

        String currentState = this.initialState;
        for (int i = 0; i < sequence.length(); i++) {
            String alphabet = String.valueOf(sequence.charAt(i));
            Pair<String, String> route = Pair.of(currentState, alphabet);
            if (transitions.containsKey(route)) {
                currentState = transitions.get(route).get(0);
            } else {
                return false;
            }
        }

        return finalStates.contains(currentState);
    }

    public List<String> getStates() {
        return states;
    }

    public List<String> getAlphabet() {
        return alphabet;
    }

    public Map<Pair<String, String>, List<String>> getTransitions() {
        return transitions;
    }

    public String getInitialState() {
        return initialState;
    }

    public List<String> getFinalStates() {
        return finalStates;
    }
}
