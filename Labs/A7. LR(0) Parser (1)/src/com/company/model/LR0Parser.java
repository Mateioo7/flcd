package com.company.model;

import java.util.ArrayList;
import java.util.List;

public class LR0Parser {
    private Grammar grammar;
    private CanonicalCollection canonicalCollection;

    public LR0Parser(Grammar grammar) {
        this.grammar = grammar;
    }

    public void computeCanonicalCollection() {
        canonicalCollection = new CanonicalCollection(new ArrayList<>());
        ParsedRule initialParsedRule = new ParsedRule("S'", List.of(grammar.getStartSymbol()), 0);
        computeClosure(List.of(initialParsedRule));
    }

    private void computeClosure(List<ParsedRule> parsedRules) {
        ParserState state = new ParserState(new ArrayList<>());
        for (ParsedRule parsedRule : parsedRules) {
            state.getParsedRules().add(parsedRule);

            if (!parsedRule.canBeParsed()) {
                continue;
            }

            if (grammar.isNonTerminal(parsedRule.getTokenAfterDot())) {
                String nonTerminal = parsedRule.getTokenAfterDot();
                List<List<String>> nonTerminalProductions = grammar.getProductionsOfNonTerminal(nonTerminal);
                for (List<String> productionRule : nonTerminalProductions) {
                    state.getParsedRules().add(new ParsedRule(nonTerminal, productionRule, 0));
                }
            }
        }

        if (canonicalCollection.contains(state)) {
            return;
        } else {
            canonicalCollection.getStates().add(state);
        }

        for (ParsedRule parsedRule : state.getParsedRules()) {
            if (parsedRule.canBeParsed()) {
                goTo(new ParsedRule(parsedRule));
            }
        }

    }

    private void goTo(ParsedRule parsedRule) {
        parsedRule.moveDotIndex();
        computeClosure(List.of(parsedRule));
    }

    public Grammar getGrammar() {
        return grammar;
    }

    public void setGrammar(Grammar grammar) {
        this.grammar = grammar;
    }

    public CanonicalCollection getCanonicalCollection() {
        return canonicalCollection;
    }

    public void setCanonicalCollection(CanonicalCollection canonicalCollection) {
        this.canonicalCollection = canonicalCollection;
    }
}
