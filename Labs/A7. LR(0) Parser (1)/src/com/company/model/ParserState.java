package com.company.model;

import java.util.List;
import java.util.Objects;

public class ParserState {
    private List<ParsedRule> parsedRules;

    public ParserState(List<ParsedRule> parsedRules) {
        this.parsedRules = parsedRules;
    }

    public List<ParsedRule> getParsedRules() {
        return parsedRules;
    }

    @Override
    public boolean equals(Object o) {
        boolean same = false;
        if (o instanceof ParserState) {
            same = this.parsedRules.equals(((ParserState) o).parsedRules);
        }

        return same;
    }

    @Override
    public int hashCode() {
        return Objects.hash(parsedRules);
    }

    @Override
    public String toString() {
        return parsedRules.toString();

//        StringBuilder sb = new StringBuilder();
//        for (ParsedRule rule : parsedRules) {
//            sb.append(rule).append("\n");
//        }
//        sb.setLength(sb.length() - 2);
//
//        return sb.toString();
    }
}
